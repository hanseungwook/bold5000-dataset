% Swap phase information of two images
% to show that phase information is highly
% critical in an image

img1 = double(rgb2gray(imread('pub5.jpg')));
img2 = double(rgb2gray(imread('airplanecabin1.jpg')));

f1 = fft2(img1);
f2 = fft2(img2);

mag1 = abs(f1);
mag2 = abs(f2);

pha1 = angle(f1);
pha2 = angle(f2);

% recompute frequency responses by swapping phase information

outimg1 = mag1 .* exp(1i*pha2);
outimg2 = mag2 .* exp(1i*pha1);

outimg1 = real(ifft2(outimg1));
outimg2 = real(ifft2(outimg2));


img1_color = imread('pub5.jpg');
img2_color = imread('airplanecabin1.jpg');

figure;
subplot(3,2,1);
imshow(img1_color, []);
title('pub5.jpg');

subplot(3,2,2);
imshow(img2_color, []);
title('airplanecabin1.jpg');

subplot(3,2,3);
imshow(img1, []);
title('pub5.jpg in grayscale');

subplot(3,2,4);
imshow(img2, []);
title('airplanecabin1.jpg in grayscale');

subplot(3,2,5);
imshow(outimg1, []);
title('pub5 phase swap');

subplot(3,2,6);
imshow(outimg2, []);
title('airplanecabin phase swap');